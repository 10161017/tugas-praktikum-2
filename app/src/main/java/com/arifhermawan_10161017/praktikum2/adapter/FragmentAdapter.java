package com.arifhermawan_10161017.praktikum2.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.arifhermawan_10161017.praktikum2.fragment.HomeFragment;
import com.arifhermawan_10161017.praktikum2.fragment.StatusFragment;

public class FragmentAdapter extends FragmentPagerAdapter {
    private Context context;
    private int tabsCounts;

    public FragmentAdapter(Context context, int tabsCounts, FragmentManager fragmentManager) {
        super(fragmentManager);

        this.context = context;
        this.tabsCounts = tabsCounts;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                StatusFragment statusFragment = new StatusFragment();
                return statusFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabsCounts;
    }
}

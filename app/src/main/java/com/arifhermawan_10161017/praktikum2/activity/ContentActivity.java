package com.arifhermawan_10161017.praktikum2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.arifhermawan_10161017.praktikum2.R;
import com.arifhermawan_10161017.praktikum2.adapter.FragmentAdapter;
import com.google.android.material.tabs.TabLayout;

public class ContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        TabLayout tlContent = findViewById(R.id.tlContent);
        ViewPager vpContent = findViewById(R.id.vpContent);

        tlContent.addTab(tlContent.newTab().setText("Home"));
        tlContent.addTab(tlContent.newTab().setText("Status"));

        tlContent.setTabGravity(TabLayout.GRAVITY_CENTER);

        FragmentAdapter adapter = new FragmentAdapter(this, tlContent.getTabCount(), getSupportFragmentManager());

        vpContent.setAdapter(adapter);
        vpContent.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tlContent));

        tlContent.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpContent.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}